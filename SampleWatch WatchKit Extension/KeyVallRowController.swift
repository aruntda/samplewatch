//
//  KeyVallRowController.swift
//  SampleWatch
//
//  Created by Arun Venkatesh on 11/22/14.
//  Copyright (c) 2014 Arun Venkatesh. All rights reserved.
//

import UIKit
import WatchKit

class KeyVallRowController: NSObject {
   
    @IBOutlet var valueLable: WKInterfaceLabel!
    @IBOutlet var keyLabel: WKInterfaceLabel!
    @IBOutlet var icon: WKInterfaceImage!
}
