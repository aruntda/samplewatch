//
//  MarketDataInterfaceController.swift
//  SampleWatch
//
//  Created by Venkatesh, Arun on 1/14/15.
//  Copyright (c) 2015 Arun Venkatesh. All rights reserved.
//

import WatchKit
import Foundation


class MarketDataInterfaceController: WKInterfaceController {

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    @IBOutlet weak var modTable: WKInterfaceTable!
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        getMODData()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    func getMODData() {
        
            var urlstr:String = "https://mobileapi.tdameritrade.wallst.com/Quote/ResearchOverview"
            var url : NSURL = NSURL(string: urlstr)!
            var request: NSURLRequest = NSURLRequest(URL:url)
            let config = NSURLSessionConfiguration.defaultSessionConfiguration()
            let session = NSURLSession(configuration: config)
            
            let task : NSURLSessionDataTask = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
                println(error)
                var jsonError:NSError?
                
                var json:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &jsonError) as NSDictionary
                var quotes:NSDictionary = json["Quotes"] as NSDictionary
                var indii:NSArray = quotes["Indices"] as NSArray
                self.modTable.setNumberOfRows(indii.count, withRowType: "mdrow")
                
                var i = 0
                for obj in indii
                {
                    var aaa:NSDictionary = indii[i] as NSDictionary
                    let row:MDRowController = self.modTable.rowControllerAtIndex(i) as MDRowController
                    row.symbol.setText(aaa["Symbol"] as NSString)
                    row.last.setText((aaa["Last"] as Double).description)
                    row.change.setText((aaa["Change"] as Double).description)
                    i++
                }

                
            });
            
            task.resume()
    }

}
