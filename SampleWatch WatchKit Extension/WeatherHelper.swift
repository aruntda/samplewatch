//
//  WeatherHelper.swift
//  ArunBuddy
//
//  Created by Arun Venkatesh on 10/21/14.
//  Copyright (c) 2014 Arun Venkatesh. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation

@objc protocol WeatherHelperDelegate{
    func weatherDataAvailable() -> Void
}

class WeatherHelper: NSObject , CLLocationManagerDelegate{
    
    let locationManager = CLLocationManager()
    var previousMSG: String = ""
    var delegate : WeatherHelperDelegate?
    var dataList:[WeatherData] = Array()
    var readOut: Bool!


    func setUpHelper(pvmsg: String, readOut : Bool){
        self.previousMSG = pvmsg
        self.readOut = readOut
        locationManager.delegate = self
        locationManager.desiredAccuracy = 20
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        locationManager.stopUpdatingLocation()

        if let location = locations.first as? CLLocation {
            var urlstr:String = ""
            urlstr = urlstr + "http://api.openweathermap.org/data/2.5/forecast?lat=" + location.coordinate.latitude.description + "&lon=" + location.coordinate.longitude.description + "&units=imperial"
            var url : NSURL = NSURL(string: urlstr)!
            var request: NSURLRequest = NSURLRequest(URL:url)
            let config = NSURLSessionConfiguration.defaultSessionConfiguration()
            let session = NSURLSession(configuration: config)
            
            let task : NSURLSessionDataTask = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
                println(error)
                var jsonError:NSError?

                var myString:String = self.previousMSG
                var json:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &jsonError) as NSDictionary

                let list:Array = json.valueForKey("list") as NSArray
                
                let tempobj = list[0]["main"] as NSDictionary
                let temp:Double = tempobj["temp"] as Double
                
                myString = myString + " Temperature is " + temp.description + " fahrenheit."
                
                let weather = list[0]["weather"] as NSArray
                let nexthrs:String = weather[0]["main"] as NSString

                myString = myString + " Weather most likely is " + nexthrs + " for next 3 hours."
                
                var isRain:Bool = false
                if nexthrs.lowercaseString.rangeOfString("rain") != nil {
                    isRain = true
                }
                self.dataList = Array()
                
                for(var i = 0 ; i < 10 ; i++)
                {
                    var wd  = WeatherData()
                    //special check for rain
                    var weather1 = list[i]["weather"] as NSArray
                    var clouds = list[i]["clouds"] as NSDictionary
                    var dtime = list[i]["dt"] as Double
                    var date = NSDate(timeIntervalSince1970: dtime)
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "h a" // superset of OP's format
                    let str = dateFormatter.stringFromDate(date)
                    
                    var mainData = list[i]["main"] as NSDictionary
                    var WindData = list[i]["wind"] as NSDictionary
                    var nexthrs1:String = weather1[0]["main"] as NSString
                    
                    wd.weather = nexthrs1
                    wd.weatherIcon = weather1[0]["icon"] as NSString
                    wd.dateTime = str
                    
                    var temp = mainData["temp"] as NSNumber
                    wd.currentTemp = temp.description
                    
                    temp = mainData["temp_max"] as NSNumber
                    wd.highTemp = temp.description
                    
                    temp = mainData["temp_min"] as NSNumber
                    wd.lowTemp = temp.description
                    
                    temp = mainData["humidity"] as NSNumber
                    wd.humidity = temp.description
                    
                    var speed = WindData["speed"] as NSNumber
                    wd.windSpeed = speed.description
                    
                    self.dataList.append(wd)
                    println(weather1)
                    if nexthrs1.lowercaseString.rangeOfString("rain") != nil {
                        isRain = true
                    }

                }
                
                if(isRain){
                    myString = myString + ". Also most likely to rain in the next 12 hours please carry your umberlla."
                }
                
                if(self.readOut == true)
                {
                    var mySpeechSynthesizer:AVSpeechSynthesizer = AVSpeechSynthesizer()
                    
                    var mySpeechUtterance:AVSpeechUtterance = AVSpeechUtterance(string:myString)
                    mySpeechUtterance.rate = 0.0500
                    println("\(mySpeechUtterance.speechString)")
                    
                    mySpeechSynthesizer.speakUtterance(mySpeechUtterance)
                }
                self.delegate?.weatherDataAvailable()
                
            });
            
            task.resume()
        }
    }
   
}
