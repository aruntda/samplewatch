//
//  InterfaceController.swift
//  SampleWatch WatchKit Extension
//
//  Created by Arun Venkatesh on 11/21/14.
//  Copyright (c) 2014 Arun Venkatesh. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBOutlet var countDownLable: WKInterfaceTimer!

    override init() {
        super.init()
        countDownLable.start()
        var timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
    }
    
    func update() {
        println("hereeee")
    }

    @IBAction func refreshPressed() {
    }
    
    override func willActivate() {
        super.willActivate()
    }

    override func didDeactivate() {
        super.didDeactivate()
    }

}
