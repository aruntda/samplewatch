//
//  WeatherData.swift
//  ArunBuddy
//
//  Created by Arun Venkatesh on 11/2/14.
//  Copyright (c) 2014 Arun Venkatesh. All rights reserved.
//

import UIKit

class WeatherData: NSObject {
   
    var highTemp:String!
    var lowTemp:String!
    var currentTemp:String!
    var weather:String!
    var weatherIcon:String!
    var dateTime:String!
    var windSpeed:String!
    var humidity:String!
    
}
