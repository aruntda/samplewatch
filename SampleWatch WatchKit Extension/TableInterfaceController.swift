//
//  TableInterfaceController.swift
//  SampleWatch
//
//  Created by Arun Venkatesh on 11/22/14.
//  Copyright (c) 2014 Arun Venkatesh. All rights reserved.
//

import UIKit
import WatchKit

class TableInterfaceController: WKInterfaceController, WeatherHelperDelegate {
    
    @IBOutlet var listTable: WKInterfaceTable!
    
    var weatHelper:WeatherHelper = WeatherHelper()

    
    override init() {
        super.init()
        weatHelper.delegate = self
        weatHelper.setUpHelper("", readOut: false);
    }
    
    func weatherDataAvailable() {
        //println(weatHelper.dataList.count)
        self.listTable.setNumberOfRows(weatHelper.dataList.count, withRowType: "row1")
        for(var i = 0; i < weatHelper.dataList.count; i++)
        {
            let row = self.listTable.rowControllerAtIndex(i) as KeyVallRowController
            let wd = weatHelper.dataList[i] as WeatherData
            row.keyLabel.setText(wd.dateTime)
            row.valueLable.setText(wd.currentTemp)
            
            row.icon.setImage(UIImage(named: wd.weatherIcon))
            
        }
    }
   
}
