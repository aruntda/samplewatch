//
//  MDRowController.swift
//  SampleWatch
//
//  Created by Venkatesh, Arun on 1/14/15.
//  Copyright (c) 2015 Arun Venkatesh. All rights reserved.
//

import UIKit
import WatchKit

class MDRowController: NSObject {
   
    @IBOutlet weak var symbol: WKInterfaceLabel!
    @IBOutlet weak var last: WKInterfaceLabel!
    @IBOutlet weak var change: WKInterfaceLabel!
}
